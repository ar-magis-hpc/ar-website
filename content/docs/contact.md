---
title: "Contact"
draft: false
---

L'action de recherche est portée par :

 - [Sébastien Rey-Coyrehourcq](https://umr-idees.fr/user/s%C3%A9bastien-rey-coyrehourcq/) (UMR IDEES) sebastien.rey-coyrehourcq [at] univ-rouen.fr
 - [Paul Chapron](https://www.umr-lastig.fr/paul-chapron/) (LASTIG, IGN-ENSG) paul.chapron [at] ign.fr
 - [Etienne Delay](https://umr-sens.fr/fr/-/etienne-delay) (Green, CIRAD) etienne.delay [at] cirad.fr
 - [Jean-Baptiste Féret](https://jbferet.gitlab.io/) (UMT TETIS, INRAE) jean-baptiste.feret [at] teledetection.fr
 - [Juste Raimbault](https://iscpif.fr/projects/juste-raimbault/) (LASTIG, IGN-ENSG) juste.raimbault [at] ign.fr
