---
title: "Description"
draft: false
---


# Description

## Contexte

Le calcul haute performance (HPC) est employé en géomatique et en géographie depuis les années 1970 (Rey-Coyrehourcq, 2015), mais il faut attendre les évolutions techniques de la période 1980-1990 et paradoxalement la démocratisation de la micro-informatique, pour qu’un discours investissant l’importance du HPC pour la géographie et la géomatique commence à émerger (Turton & Openshaw, 1998; Openshaw & Abrahart, 2000).

Dans un contexte d’évolution technologique rapide (nature et volumétrie des données accessibles, complexification des modèles associées, etc.), et alors même que la ressource HPC est disponible partout sur le territoire (Genci et Equip@Meso avec 17 centres régionaux), l’usage du HPC est rare. Après une rapide enquête dans le milieu de la simulation en géographie (voir la session 3 des Journée du CAlcul et Données (JACD) 2019), la communauté semble se limiter à quelques personnes et quelques laboratoires. Est-ce la partie émergée de l’iceberg ? Qu’en est-il de la géomatique ?

La difficulté d’obtenir un aperçu des pratiques HPC en simulation spatiale reflète l’absence d’une fédération autour de cette thématique au niveau national. On peut en inférer que ces communautés, lorsqu’elles existent, sont fragmentées et condamnées à une vision du HPC par le seul prisme de leurs pratiques (simulation, télédétection, etc.).

En 1998 OpenShaw & Turton avaient identifié les avantages d’intégrer le HPC comme un paradigme supplémentaire à disposition des géomaticiens :

  – la parallélisation des méthodes existantes
  – la remobilisation de méthodes passées, abandonnées pour des raisons techniques
  – l’augmentation de la taille des jeux de données admissibles et la précision de leurs analyses
  – l’emploi et le développement de méthodes rendues possibles par l’infrastructure HPC

Toujours pertinents aujourd’hui, ces items sont de nature transversale, susceptibles d’enrichir les actions de recherches existantes.

À ces pistes d’explorations, qu’il faut prendre dans leur dimension épistémologique (nouvelles connaissances, nouvelles problématiques abordables) il conviendrait d’ajouter aujourd’hui celle de l’éthique et de la responsabilité écologique dans l’usage de telles ressources.

L’enjeu est en premier lieu de fédérer une communauté HPC actuellement fragmentée autour de pratiques HPC très diverses, diversité qu’il nous faudra inventorier. Dans un second temps, et lorsque ce paysage sera mieux dessiné, nous pourrons envisager de porter les actions de formation et d’information nécessaires à un usage plus démocratisé du HPC en géomatique.

## Objectifs

Le **premier objectif**, qui recouvre aussi la principale difficulté de cette AP, est d’offrir un point d’ancrage susceptible d’attirer et de fédérer la communauté de géomaticiens faisant usage du HPC.

La constitution d’une telle communauté ne pourra se faire qu’en misant sur une large communication. Dans un premier temps il s’agira d’organiser des sessions dédiées au retours d’expériences afin d’identifier la diversité des pratiques actuelles et de créer des espaces de discussions nécessaires aux transferts (méthodologiques, théoriques, pratiques) entre ces communautés. Nous proposons de concrétiser ces échanges dans un position paper qui offrira un premier panorama des usages, des pratiques, mais aussi des problématiques rencontrés par les différentes communautés, par exemple autour de l’accès différencié aux outils et aux ressources.

Deux axes seront a priori envisagés pour positionner les sujets des premières discussions :

  – le type de limitation auquel font face les scientifiques. Elles peuvent être de nature technique : temps de calcul, taille mémoire, bande passante ... mais aussi humaines : accès à la ressources HPC, technologies complexes à maîtriser, ...
  – la nature et le format de l’information spatiale mobilisée : couches vecteurs, rasters, LIDAR, données tabulaires, ...


Ce position paper servira d’appui au **second objectif**: présenter aux disciplines qui utilisent l’information spatiale l’apport du HPC dans leurs pratiques. Il s’agira donc de montrer ou démontrer, via des ateliers et des working paper, dans quelle mesure le HPC est devenu plus accessible, mais aussi en quoi il est porteur d’accélération ou d’innovation.

Enfin un **troisième objectif**, peut-être plus difficile à atteindre, serait d’intégrer à l’Action Recherche des échanges avec la communauté internationale des géomaticiens utilisant le HPC, en invitant par exemple des chercheurs étrangers à présenter leurs travaux, ou bien en intégrant un volet comparatif international dans notre position paper.

## Programme d'actions

Faisant face à une hétérogénéité de pratiques et de communautés, nous proposons de conserver une certaine flexibilité dans la mise en place d’actions. Néanmoins, pour lancer la dynamique et animer la petite communauté que nous avons déjà identifiée et fédérée derrière cette AP, nous proposons ce programme prévisionnel :

 - Année 1 : Élaboration d’un questionnaire à destination de la communauté géomatique nationale afin d’identifier des utilisateurs de HPC et faire remonter par leur biais des informations sur des équipes internationales actives ou des projets passés ou en cours. En résumé, elle aura pour ambition : de répertorier les communauté d’utilisateurs ; d’identifier les pratiques ; d’identifier les usages ; avec un lancement du questionnaire et de l’AP à SAGEO 2021.

 - Année 2 : Préparer et conduire des entretiens avec des personne à l’interface entre l’accès au calcul et les utilisateurs. Nous mettrons en discussions les résultats de l’enquête. Nous nous questionnerons sur l’adéquation entre offre et demande (~20 entretiens). Mettre en place et animer un site internet avec un espace de discussion.

 - Année 3 : Un coding-camp/école thématique qui permettra aux communautés de se rencontrer. Participation à Sagéo (conférences, ateliers) pour restituer les interactions entre les communautés. Rédaction d’un position paper sur l’opportunité de l’usage des HPC pour des disciplines utilisant l’information spatiale.

## Bibliographie

Turton I, Openshaw S. High-Performance Computing and Geography: Developments, Issues, and Case Studies. Environment and Planning A: Economy and Space. 1998;30(10):1839-1856. doi:10.1068/a301839

Openshaw, S., & Abrahart, R. J. (2000). GeoComputation. Taylor & Francis.

Sébastien Rey-Coyrehourcq. Une plateforme intégrée pour la construction et l'évaluation de modèles de simulation en géographie. Géographie. Université Panthéon-Sorbonne - Paris I, 2015. Français. ⟨NNT : 2015PA010690⟩. ⟨tel- 01652092v2⟩
