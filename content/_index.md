---
title: "Accueil"
draft: false
---

# Usages du calcul hautes performances en géomatique

L'Action de Recherche 13 du [GDR MAGIS](https://gdr-magis.imag.fr/) (Groupement de Recherche CNRS, Méthodes et Applications pour la Géomatique et l'Information Spatiale), Usages du Calcul Hautes Performances en Géomatique, vise à fédérer la communauté des géomaticiens utilisateurs de HPC.

Sa première action, en cours d'élaboration (Octobre 2022), est une cartographie des usages effectifs, via un questionnaire en ligne diffusé dans la communauté.

Le description détaillée du projet de l'AR est disponible [sur cette page]({{< relref "/docs/about" >}}), et la liste des chercheurs porteurs du projet [ici]({{< relref "/docs/contact" >}}).
